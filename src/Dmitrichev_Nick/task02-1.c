#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
int Chain_Len(char *Str) 
{
	int Count = 1, i = 0, Length = 0, Res = 0;
	Length = strlen(Str);
	while (i < Length)
	{
		if (Str[i + 1] == Str[i])
			Count++;
		else
		{
			if (Count > Res)
				Res = Count;
			Count = 1;
		}
		i++;
	}
	return Res; //���������� 0, ���� ������ ������
}
void Demonstration()
{
	srand(time(NULL));
	char Str[11];	
	int Num=11;
	char i=0;
	for (i = 0;i < (Num-1);i++)
		Str[i] = 48 + rand() % 10;
	Str[i] = '\0';
	puts("----DEMONSTRATION BEGINS----\n");
	puts("Function 'Chain_Len defines' the longest sequence of the repeating numbers in\nthe numerical string, and returns its length\nThe string is filled with casual figures\n");
	puts(Str);
	printf("Result: %i\n", Chain_Len(Str));
	puts("\n----DEMONSTRATION IS OVER---\n");
}
int main()
{	
	Demonstration();
	return 0;
}