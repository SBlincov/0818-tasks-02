//
//  main.c
//  Task02-1
//
//  Created by Blincov Sergey on 17.05.16.
//  Copyright © 2016 Blincov Sergey. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char * argv[]) {
    srand(22);
    int array[1000];
    for (int i=0;i<10;i++){
        array[i]=0+rand()%10;
    }
    for (int i = 0;i<10;i++){
        printf("%d элемент = %d \n",i,array[i]);
    }
	
	//Realisation here
    int countOfLenght=0;
    int tmp = array[0];
    int result=1;
    for(int i=0;i<10;i++){
        tmp = array[i+1];
        if (tmp == array[i]) {
			countOfLenght+=1;
		} else{
			if (countOfLenght>result){
				result = countOfLenght;
				countOfLenght = 1;
			}
		}
    }
    printf("Длина: %d",result);
    return 0;
}
