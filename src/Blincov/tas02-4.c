//
//  main.c
//  Task02-4
//
//  Created by Blincov Sergey on 30.05.16.
//  Copyright © 2016 Blincov Sergey. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int rowWithMaxSumElemets(int n,int (*arr)[n]);

int main(){
	int arr[3][3];
	srand(time(0));
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			arr[i][j]=rand()%10;
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
	printf("Row with maximal sum of elements: \n");
	for(int i=0;i<3;i++)
		printf("%d\n",arr[i][rowWithMaxSumElemets(3,arr)]);
	printf("\n");
	return 0;
}
int rowWithMaxSumElemets(int n,int (*arr)[n]){
	int i,j,max=0,sum;
	int result=0;
	for(i=0;i<n;i++){
		sum=0;
		for(j=0;j<n;j++)
			sum=sum+arr[j][i];
		if(sum>max){
			max=sum;
			result=i;
		}
	}
	return result;
}