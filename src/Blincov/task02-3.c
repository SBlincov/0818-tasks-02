//
//  main.c
//  Task02-3
//
//  Created by Blincov Sergey on 30.05.16.
//  Copyright © 2016 Blincov Sergey. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int counter(int(*arr)[3], int n);

int main(){
	int arr[3][3];
	srand(time(0));
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			arr[i][j]=rand()%10;
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
	printf("Quantity of repeat = ");
	for(int i=0;i<3;i++)
		printf("%d ",arr[counter(arr,3)][i]);
	printf("\n");
	return 0;
}
int counter(int (*arr)[3], int n){
	int a,b,i,j,max=0;
	int numberOfString=0;
	for(i=0;i<n;i++){
		a=0;
		for(j=0;j<n;j++){
			for(b=0;b<n;b++)
				if((arr[i][j]==arr[i][b])&&(j!=b))
					a=a+1;
		}
		if(a>max){
			max=a;
			numberOfString=i;
		}
	}
	return numberOfString;
}
