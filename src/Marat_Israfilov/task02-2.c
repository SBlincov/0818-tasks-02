/*Программа проверяющая строку на палиндром*/
#include <stdio.h>
#include <string.h>
#define N 255

int str_palindrome(char *str, int len)
{
	int i;
	int result = 1;
	for(i = 0; i <  (len-i); i++)
	{				
		while(str[i] == ' ')
		{
			i++;
			len++;
		}
		while(str[len-i] == ' ')
		{
			len--;		
		}
		if(str[i] != str[len-i])
		{			
			result = 0;
			break;
		}		
	}
	return result;
}

int main()
{
	char str[N];
	int len;
	printf("Enter the string:\n");
	fgets(str, N, stdin);
	len = strlen(str)-1;
	printf("Palindrom: %d\n", str_palindrome(str, len));
	return 0;
}
