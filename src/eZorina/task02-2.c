#include <stdio.h>
#include <string.h>
#include <conio.h>

int palindr(char *str)
{
	int i,j;
	for (i=0, j=strlen(str)-2; i<j; i++, j--)
		if (str[i]!=str[j])
			return 0;
	return 1;
}

int main()
{
	char str[100];
	puts("Enter a string\n");
	fgets(str,100,stdin);
	if (palindr(str)==1)
		puts("This string is palindrome");
	else
		puts("This string is not palindrome");
	getch();
	return 0;
}