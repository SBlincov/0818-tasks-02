#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#define	n	10

int maxstr(int *arr)
{
	int i, j, max=0, count=0;
	for (i=0; i<n-1; i++)
	{
		for (j=i+1; j<n; j++)
			if (arr[i]==arr[j])
				count++;
		if (count>max)
			max=count;
		count=0;
	}
	return max;
}

int maxcount(int (*p)[n])
{
	int i, max=0, str;
	for (i=0; i<n; i++)
	{
		if (maxstr(p[i])>max)
		{
			max=maxstr(p[i]);
			str=i;
		}
	}
	return str;
}

int main()
{
	int matr[n][n];
	int i,j;
	srand(time(0));
	for (i=0; i<n; i++)
		for (j=0; j<n; j++)
			matr[i][j]=rand()%13;
	printf("Your matrix:\n");
	for (i=0; i<n; i++)
	{
		for (j=0; j<n; j++)
			printf("%5d",matr[i][j]);
		printf("\n");
	}
	printf("String number: %d", maxcount(matr)+1);
	getch();
	return 0;
}